﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace lab8_2
{
    public partial class cursosForm : Form
    {
        SqlConnection con;
        DataSet ds = new DataSet();
        DataTable tablePerson = new DataTable();
        BindingManagerBase managerBase;
        SqlCommandBuilder builder;
        SqlDataAdapter adapter = new SqlDataAdapter();


        public cursosForm()
        {
            InitializeComponent();
        }

        private void cursosForm_Load(object sender, EventArgs e)
        {

            String str = "Server=DESKTOP-GMU23Q8;DataBase=School;Integrated Security=true;";
            con = new SqlConnection(str);
            String sql = "SELECT * FROM Course";
            SqlCommand cmd = new SqlCommand(sql, con);
            adapter.SelectCommand = cmd;
            adapter.Fill(ds, "Course");
            tablePerson = ds.Tables["Course"];
            dgvListado.DataSource = tablePerson;

            txtCourseID.DataBindings.Add("text", tablePerson, "CourseID");
            txtTitle.DataBindings.Add("text", tablePerson, "Title");
            txtCredits.DataBindings.Add("text", tablePerson, "Credits");
            txtDepartmentID.DataBindings.Add("text", tablePerson, "DepartmentID");

            managerBase = this.BindingContext[tablePerson];
        }

        private void btnPerson_Click(object sender, EventArgs e)
        {
            Form1 personas= new Form1();
            personas.Show();
            this.Hide();
        }

        private void btnDepartments_Click(object sender, EventArgs e)
        {
            Form2 departments = new Form2();
            departments.Show();
            this.Hide();
        }

        private void btnPrimero_Click(object sender, EventArgs e)
        {

            managerBase.Position = 0;
        }

        private void btnAnterior_Click(object sender, EventArgs e)
        {

            managerBase.Position -= 1;
        }

        private void btnSiguiente_Click(object sender, EventArgs e)
        {

            managerBase.Position += 1;
        }

        private void btnUltimo_Click(object sender, EventArgs e)
        {

            managerBase.Position = managerBase.Count;
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {

            managerBase.AddNew();
        }

        private void btnInsertar_Click(object sender, EventArgs e)
        {

            try
            {
            managerBase.EndCurrentEdit();
            builder = new SqlCommandBuilder(adapter);
            adapter.Update(tablePerson);
            MessageBox.Show("Curso agregado correctamente");
            }catch(Exception error)
            {
                MessageBox.Show("Error al insertar el registro.");
            }
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {


            managerBase.EndCurrentEdit();
            builder = new SqlCommandBuilder(adapter);
            adapter.Update(tablePerson);
            MessageBox.Show("Se modificó el registro correctamente");
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {

            managerBase.RemoveAt(managerBase.Position);
            builder = new SqlCommandBuilder(adapter);
            adapter.Update(tablePerson);
            MessageBox.Show("Curso eliminada correcmanete");
        }
    }
}
